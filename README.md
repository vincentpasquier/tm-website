# TM Website

Master thesis website ([example](http://vincent.pasquier.home.hefr.ch)).

## Development
To start development from repository, install [node](http://nodejs.org/) and [yo](http://yeoman.io/).
```
curl https://raw.github.com/creationix/nvm/master/install.sh | sh
nvm install 0.10
nvm alias default 0.10
npm install -g yo
```

Once installed, run `npm install` and `bower install`. To start server run `grunt server`. To build run `grunt build`.

## Deployment
To deploy the website, configure URLs in `app/scripts/app.js` to access each files. Multiple-part file are also available by
declaring a string Array shown as following.
```
[ "multiple-url1.json", "multiple-url2.json" ]
```

File structure should follow example given in `example` directory.