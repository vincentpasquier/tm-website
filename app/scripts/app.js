'use strict';

var app = angular.module ( 'MasterWebsite', [ 'chieffancypants.loadingBar', 'ngAnimate', 'ngRoute', 'ngSanitize', 'ui.bootstrap', 'AsyncLoader'] )
	.config ( function ( $routeProvider ) {
	$routeProvider
		.when ( '/', {
		templateUrl: 'views/main.html',
		controller : 'MainCtrl'
	} )
		.otherwise ( {
		redirectTo: '/'
	} );
} );

app.controller ( 'MainCtrl', [ '$scope', '$location', '$anchorScroll', function ( $scope, $location, $anchorScroll ) {
		/**
		 * Based on http://stackoverflow.com/questions/14712223/how-to-handle-anchor-hash-linking-in-angularjs
		 * @param id
		 */
		$scope.scrollTo = function ( id ) {
			var old = $location.hash ();
			$location.hash ( id );
			$anchorScroll ();
			//reset to old to keep any additional routing logic from kicking in
			$location.hash ( old );
		}
	}] )
	.factory ( 'urls', [ function () {
		var urls = {
			'files'     : 'files.json',
			'tasks'     : 'tasks.json',
			'minutes'   : 'minutes.json',
			'references': 'references.json',
			'wiki'      : 'wiki.json'
		};
		return urls;
	}] )
	.factory ( 'parseURI', [ function () {
		/**
		 * Based on http://blog.stevenlevithan.com/archives/parseuri
		 * @type {{parseURI: Function, options: {strictMode: boolean, key: Array, q: {name: string, parser: RegExp}, parser: {strict: RegExp, loose: RegExp}}}}
		 */
		var parser = {
			parseURI: function ( str ) {
				var o = parser.options,
					m = o.parser[o.strictMode ? "strict" : "loose"].exec ( str ),
					uri = {},
					i = 14;

				while ( i-- ) uri[o.key[i]] = m[i] || "";

				uri[o.q.name] = {};
				uri[o.key[12]].replace ( o.q.parser, function ( $0, $1, $2 ) {
					if ( $1 ) uri[o.q.name][$1] = $2;
				} );

				return uri;
			},

			options: {
				strictMode: false,
				key       : ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
				q         : {
					name  : "queryKey",
					parser: /(?:^|&)([^&=]*)=?([^&]*)/g
				},
				parser    : {
					strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
					loose : /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
				}
			}
		};
		return parser;
	}] )
	.
	filter ( 'parseFileName', [ 'parseURI', function ( parser ) {
		return function ( data ) {
			var uri = parser.parseURI ( data );
			return uri.file;
		}
	}] );