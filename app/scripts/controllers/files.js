'use strict';

/**
 * Manages Views and their data.
 */
app.controller ( 'FileCtrl', [ '$scope', 'urls', function ( $scope, urls ) {

	/**
	 * Each component initializes itself with their name to lookup url[name]
	 * @param name
	 */
	$scope.initName = function ( name ) {
		$scope.name = name;
		$scope.url = urls[$scope.name];
	}

	/**
	 * Object-Array containing data
	 * @type {Array}
	 */
	$scope.data = [];

	/**
	 * Latest element in data Array
	 * @type {{}}
	 */
	$scope.latest = {};

	/**
	 * Selected element in data Array
	 * @type {{}}
	 */
	$scope.selected = {};

	/**
	 * Action performed when selecting item
	 * @param tasks
	 */
	$scope.selectItem = function ( item ) {
		$scope.selected = item;
	}

	/**
	 * Watches $scope.data to react when receiving Asynchronous data.
	 */
	$scope.$watchCollection ( 'data', function ( n, o ) {
		if ( n === o ) {
			return;
		}
		if ( angular.isArray ( $scope.data ) ) {
			var oldest = 0;
			angular.forEach( $scope.data, function ( obj, i ) {
				var dateA = new Date ( $scope.data[oldest].date ),
					dateB = new Date ( obj.date );
				if ( dateA < dateB ) {
					oldest = i;
				}
			});
			$scope.latest = $scope.data[oldest];
			$scope.selected = $scope.latest;
		}
	} );

}] );