'use strict';

/**
 * Manages Asynchronous loading of resources
 * Requires :
 * - data: two-way binding to set data to parent
 * - url: two-way binding to get data from
 * You can specify multiple loading by adding 'true' parameter to element-visible directive.
 */
angular.module ( 'AsyncLoader', [] )
	.directive ( 'elementVisible', [ '$http', '$window', '$timeout', function ( $http, $window, $timeout ) {
		return {
			restrict: 'A',
			scope   : {
				data: '=data',
				url : '=url'
			},
			link    : function ( scope, element, attrs ) {

				/**
				 * Window to listen 'scroll' event.
				 * @type {*}
				 */
				$window = angular.element ( $window );

				/**
				 * Element to watch if visible.
				 * @type {*}
				 */
				element = angular.element ( element );

				/**
				 * Indicate if the element has been loaded.
				 * @type {boolean}
				 */
				var loaded = false;

				/**
				 * Checks whether the element is visible or not.
				 */
				scope.isElementVisible = function () {
					var docViewTop = $window.scrollTop ();
					var docViewBottom = docViewTop + $window.height ();

					var elemTop = element.offset ().top;
					var elemBottom = elemTop + element.height ();

					return (elemBottom <= docViewBottom) && (elemTop >= docViewTop);
				};

				/**
				 * Reacts to scroll based on its visibility and loading status
				 */
				scope.reactScroll = function () {
					var visible = scope.isElementVisible ();
					if ( visible && !loaded ) {
						loaded = true;
						$http.get ( scope.url ).success (function ( data ) {
							if ( scope.$eval ( attrs.elementVisible ) ) {
								scope.loadMultiple ( data );
							} else {
								scope.data = data;
							}
						} ).error ( function ( error ) {
							loaded = false;
							// TODO: Might think of something
						} );
					}
				}

				/**
				 * Loads multiple part file.
				 * @param data
				 */
				scope.loadMultiple = function ( multiPart ) {
					var toPush = [];
					angular.forEach ( multiPart, function ( obj ) {
						$http.get ( obj ).success ( function ( data ) {
							toPush.push ( data );
						} );
					} );
					scope.data = toPush;
				}

				/**
				 * Reacts to scroll event.
				 */
				$window.on ( 'scroll', function () {
					scope.reactScroll ();
					scope.$apply ();
				} );

				/**
				 * At initialization, gets
				 */
				$timeout ( function () {
					scope.reactScroll ();
				}, 0 );
			}
		};
	} ] );